package com.novuyo.Dao;


import com.novuyo.Entity.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

@Repository("mysql_request")
public class MySqlRequestDaoImpl implements RequestDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static class UserRowMapper implements RowMapper<Request> {

        @Override
        public Request mapRow(ResultSet resultSet, int i) throws SQLException {
            Request request = new Request();
            request.setId(resultSet.getInt("id"));
            request.setUsername(resultSet.getString("username"));
            request.setPassword(resultSet.getString("password"));
            request.setRecipientid(resultSet.getString("recipientid"));
            //request.setRecipientmobile(resultSet.getString("recipientmob"));
            //request.setDeliveradd(resultSet.getString("deliveryadd"));
            request.setCollectadd(resultSet.getString("collectadd"));
            //request.setDesc(resultSet.getString("description"));
            request.setExcess(resultSet.getString("excess"));
            request.setPaytype(resultSet.getString("paytype"));
            request.setTotal(resultSet.getString("total"));
            request.setRepmob(resultSet.getString("repmob"));
            request.setDescription(resultSet.getString("description"));
            request.setDeliveryaddress(resultSet.getString("deliveryaddress"));
            return request;
        }
    }

    @Override
    public Collection<Request> getAllRequests() {

        //final String sql = "SELECT id,username,password,recipientid,recipientmob,deliveryadd,collectadd,description,excess,paytype,total FROM requests";
        final String sql = "SELECT id,username,password,recipientid,collectadd,excess,paytype,total,repmob,description,deliveryaddress FROM requests";
        List<Request> request = jdbcTemplate.query(sql, new UserRowMapper());
        return request;
    }

    @Override
    public Request getRequestById(int id) {
        //final String sql = "SELECT id,username,password,recipientid,recipientmob,deliveryadd,collectadd,description,excess,paytype,total FROM requests where id = ?";
        final String sql = "SELECT id,username,password,recipientid,collectadd,excess,paytype,total,repmob,description,deliveryaddress FROM requests where id = ?";
        Request request = jdbcTemplate.queryForObject(sql, new UserRowMapper(), id);
        return request;
    }

    @Override
    public void removeRequestById(int id) {
        final String sql = "DELETE FROM requests WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void updateRequest(Request request) {

        //final String sql = "UPDATE requests SET username=?,password=?,recipientid=?,recipientmob=?,deliveryadd=?,collectadd=?,description=?,excess=?,paytype=?,total=? WHERE id= ?";
        final String sql = "UPDATE requests SET username=?,password=?,recipientid=?,collectadd=?,excess=?,paytype=?,total=?,repmob=?,description=?,deliveryaddress=? WHERE id= ?";
        int id = request.getId();
        String username = request.getUsername();
        String password = request.getPassword();
        String recipientid = request.getRecipientid();
        //String recipientmob = request.getRecipientmobile();
        //String deliveryadd = request.getDeliveradd();
        String collectadd = request.getCollectadd();
        //String description = request.getDesc();
        String excess = request.getExcess();
        String paytype = request.getPaytype();
        String total = request.getTotal();
        String repmob = request.getRepmob();
        String description = request.getDescription();
        String deliveryaddress = request.getDeliveryaddress();
        //jdbcTemplate.update(sql, new Object[]{username, password, recipientid, recipientmob, deliveryadd, collectadd, description, excess, paytype, total, id});
        jdbcTemplate.update(sql, new Object[]{username, password, recipientid, collectadd, excess, paytype, total,repmob,description,deliveryaddress,id});
    }

    @Override
    public void insertRequestToDb(Request request) {

        //final String sql = "INSERT INTO requests (username, password, recipientid,recipientmob,deliveryadd,collectadd,description,excess,paytype,total) VALUES (?,?,?,?,?,?,?,?,?,?)";
        final String sql = "INSERT INTO requests (username, password, recipientid,collectadd,excess,paytype,total,repmob,description,deliveryaddress) VALUES (?,?,?,?,?,?,?,?,?,?)";
        String username = request.getUsername();
        String password = request.getPassword();
        String recipientid = request.getRecipientid();
        //String recipientmob = request.getRecipientmobile();
        //String deliveryadd = request.getDeliveradd();
        String collectadd = request.getCollectadd();
        //String description = request.getDesc();
        String excess = request.getExcess();
        String paytype = request.getPaytype();
        String total = request.getTotal();
        String repmob = request.getRepmob();
        String description = request.getDescription();
        String deliveryaddress = request.getDeliveryaddress();
        //jdbcTemplate.update(sql, new Object[]{username, password, recipientid,recipientmob,deliveryadd,collectadd,description,excess,paytype,total});
        jdbcTemplate.update(sql, new Object[]{username, password, recipientid,collectadd,excess,paytype,total,repmob,description,deliveryaddress});
    }

}