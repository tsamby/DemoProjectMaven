package com.novuyo.Dao;


import com.novuyo.Entity.Request;
import java.util.Collection;

public interface RequestDao {

    Collection<Request> getAllRequests();

    Request getRequestById(int id);

    void removeRequestById(int id);

    void updateRequest(Request request);

    void insertRequestToDb(Request request);

}
