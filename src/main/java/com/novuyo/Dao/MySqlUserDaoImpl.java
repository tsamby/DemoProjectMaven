package com.novuyo.Dao;


import com.novuyo.Entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

@Repository("mysql_user")
public class MySqlUserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;



   public static class UserRowMapper implements RowMapper<User>{

        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setEmail(resultSet.getString("email"));
            user.setUsername(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
            user.setMobile(resultSet.getString("mobile"));
            return user;
        }
    }





    @Override
    public Collection<User> getAllUsers() {

        final String sql = "SELECT id,email,username,password,mobile FROM user";
        List<User> user = jdbcTemplate.query(sql,new UserRowMapper());
        return user;
    }

    @Override
    public User getUserById(int id) {
        final String sql = "SELECT id,email,username,password,mobile FROM user where id = ?";
        User user = jdbcTemplate.queryForObject(sql, new UserRowMapper(), id);
        return user;
    }

    @Override
    public User getUserByEmail(String email) {
        final String sql = "SELECT id,email,username,password,mobile FROM user where email = ?";
        User user = jdbcTemplate.queryForObject(sql, new UserRowMapper(), email);

        return user;
    }

    @Override
    public void removeUserById(int id) {
        final String sql = "DELETE FROM user WHERE id = ?";
        jdbcTemplate.update(sql,id);
    }

    @Override
    public void updateUser(User user) {

        final String sql = "UPDATE user SET email =?, username =?,password =?,mobile =? WHERE id= ?";
        int id = user.getId();
        String email = user.getEmail();
        String username = user.getUsername();
        String password = user.getPassword();
        String mobile = user.getMobile();
        jdbcTemplate.update(sql, new Object[]{email,username,password,mobile,id});

    }

    @Override
    public void insertUserToDb(User user) {

        final  String sql = "INSERT INTO user (email,username,password,mobile) VALUES (?,?,?,?)";
        String email = user.getEmail();
        String username = user.getUsername();
        String password = user.getPassword();
        String mobile = user.getMobile();
        jdbcTemplate.update(sql, new Object[]{email,username,password,mobile});
    }
}
