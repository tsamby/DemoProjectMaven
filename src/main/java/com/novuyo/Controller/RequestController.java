package com.novuyo.Controller;

import com.novuyo.Entity.Request;
import com.novuyo.Service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.Collection;

@RestController
@RequestMapping("/requests")
public class RequestController {

    @Autowired
    private RequestService requestService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Request> getAllRequests(){
        return  requestService.getAllRequests();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Request getByRequestId(@PathVariable("id") int id){
        return requestService.getRequestById(id);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRequestById(@PathVariable("id") int id){
        requestService.removeRequestById(id);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateRequest(@RequestBody Request request){
        requestService.updateRequest(request);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void insertStudent(@RequestBody Request request){
        System.out.println("request ---->" + request.getUsername());
        System.out.println("request ---->" + request.getPassword());
        System.out.println("request ---->" + request.getRecipientid());
        //System.out.println("request ---->" + request.getRecipientmobile());
        //System.out.println("request ---->" + request.getDeliveradd());
        System.out.println("request ---->" + request.getCollectadd());
        //System.out.println("request ---->" + request.getDesc());
        System.out.println("request ---->" + request.getExcess());
        System.out.println("request ---->" + request.getPaytype());
        System.out.println("request ---->" + request.getTotal());
        System.out.println("request ---->" + request.getRepmob());
        requestService.insertRequest(request);
    }
}