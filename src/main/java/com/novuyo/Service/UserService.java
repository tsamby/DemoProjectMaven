package com.novuyo.Service;


import com.novuyo.Dao.UserDao;
import com.novuyo.Entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class UserService {
    @Autowired
    @Qualifier("mysql_user")
    private UserDao userDao;

    public Collection<User> getAllUsers(){
        return  userDao.getAllUsers();
    }

    public User getUserById(int id){
        return this.userDao.getUserById(id);
    }

    public User getUserByEmail(String email){
        return this.userDao.getUserByEmail(email);
    }

    public void removeUserById(int id){
        this.userDao.removeUserById(id);
    }

    public void updateUser(User user){
        this.userDao.updateUser(user);
    }

    public void insertUser(User user) {
        this.userDao.insertUserToDb(user);
    }
}