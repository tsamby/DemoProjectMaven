package com.novuyo.Service;

import com.novuyo.Dao.RequestDao;
import com.novuyo.Entity.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class RequestService {

    @Autowired
    @Qualifier("mysql_request")
    private RequestDao requestDao;

    public Collection<Request> getAllRequests(){
        return  requestDao.getAllRequests();
    }

    public Request getRequestById(int id){
        return this.requestDao.getRequestById(id);
    }

    public void removeRequestById(int id){
        this.requestDao.removeRequestById(id);
    }

    public void updateRequest(Request request){
        this.requestDao.updateRequest(request);
    }

    public void insertRequest(Request request) {
        this.requestDao.insertRequestToDb(request);
    }
}
