package com.novuyo.Entity;

public class Request {
    private int id;
    private String username;
    private String password;
    private String recipientid;
    private String repmob;
    //private String recipientmob;
    //private String deliveradd;
    private String collectadd;
    //private String desc;
    private String excess;
    private String paytype;
    private String total;
    private String description;
    private String deliveryaddress;

    //public Request(int id, String username, String password, String recipientid, String recipientmob, String deliveradd, String collectadd, String desc, String excess,String paytype, String total) {

        public Request(int id, String username, String password, String recipientid, String collectadd, String excess,String paytype, String total,String repmob,String description, String deliveryaddress) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.recipientid = recipientid;
        //this.recipientmob = recipientmob;
        //this.deliveradd = deliveradd;
        this.collectadd = collectadd;
        //this.desc = desc;
        this.excess = excess;
        this.paytype = paytype;
        this.total = total;
        this.repmob = repmob;
        this.description = description;
        this.deliveryaddress = deliveryaddress;

    }

    public Request(){

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeliveryaddress() {
        return deliveryaddress;
    }

    public void setDeliveryaddress(String deliveryaddress) {
        this.deliveryaddress = deliveryaddress;
    }

    public String getRepmob() {
        return repmob;
    }

    public void setRepmob(String repmob) {
        this.repmob = repmob;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRecipientid() {
        return recipientid;
    }

    public void setRecipientid(String recipientid) {
        this.recipientid = recipientid;
    }

   /*public String getRecipientmobile() {
        return recipientmob;
    }


    public void setRecipientmobile(String recipientmob) {
        this.recipientmob = recipientmob;
    }

    public String getDeliveradd() {
        return deliveradd;
    }

    public void setDeliveradd(String deliveradd) {
        this.deliveradd = deliveradd;
    }
    */

    public String getCollectadd() {
        return collectadd;
    }

    public void setCollectadd(String collectadd) {
        this.collectadd = collectadd;
    }

    /*public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    */

    public String getExcess() {
        return excess;
    }

    public void setExcess(String excess) {
        this.excess = excess;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
